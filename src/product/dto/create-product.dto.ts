import { ParseIntPipe } from '@nestjs/common';
import { Transform } from 'class-transformer';
import { IsNumber, IsString } from 'class-validator';

export class CreateProductDTO {
    @IsString()
    name: string;

    @IsNumber()
    @Transform(price => parseInt(price), {toClassOnly: true})
    price: number;

    @IsString()
    description: string;
}

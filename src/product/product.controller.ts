import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, ParseIntPipe, Patch, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { CreateProductDTO } from './dto/create-product.dto';
import { Product } from './product.entity';
import { ProductService } from './product.service';

@Controller('product')
export class ProductController {
    constructor (
        private productService: ProductService
    ) {}

    @Get('/all')
    public async getProducts(): Promise<Product[]> {
        return await this.productService.getProducts();
        
    }

    @Get('/:id')
    public async getProductById(@Param('id', ParseIntPipe) id: number) : Promise<Product>{
        return this.productService.getProduct(id)
    }

    @Delete('/:id')
    public async deleteProductById(@Param('id', ParseIntPipe) id: number) {
        return this.productService.deleteProduct(id)
    }

    @Post()
    @UsePipes(ValidationPipe)
    public async createProduct(@Body() createProductDto: CreateProductDTO) : Promise<Product> {
        return this.productService.createProduct(createProductDto)
    }

    @Patch('/:id')
    public async updateProduct(
        @Param('id', ParseIntPipe) id: number,
        @Body(ValidationPipe) createProductDto: CreateProductDTO,
    ) {
        return this.productService.editProduct(id, createProductDto)
    }

}

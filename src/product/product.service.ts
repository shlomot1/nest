import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './product.entity';
import { CreateProductDTO } from './dto/create-product.dto';
import { ProductRepository } from './product.repository';

@Injectable()
export class ProductService {
  
    constructor(
        @InjectRepository(ProductRepository)
        private productRepository: ProductRepository,
    ) { }


    public async createProduct(
        createProductDto: CreateProductDTO,
    ): Promise<Product> {
        return await this.productRepository.createProduct(createProductDto) //create product
    }


    public async getProducts(): Promise<Product[]> {
        return await this.productRepository.find() ;// find all
    }


    public async getProduct(id: number): Promise<Product> {
        const foundProduct = await this.productRepository.findOne(id)  //findOne
        if (!foundProduct) {
            throw new NotFoundException('Product not found');
        }
        return foundProduct;
    }


    public async editProduct(
        productId: number,
        createProductDto: CreateProductDTO,
    ): Promise<Product> {
        //find One and Edit
        const product = await this.getProduct(productId) ; //find One

        return this.productRepository.editProduct(createProductDto, product) ; // Edit Product
    }


    public async deleteProduct(id: number): Promise<void> {
        const deleteResult = await this.productRepository.delete(id)

        if (deleteResult.affected === 0) {
            throw new NotFoundException(`Task with ID ${id} not found`);
        }

    }
}
